use super::MatrixTrait;
use std::iter::Sum;
use std::ops::{Add, AddAssign, Deref, DerefMut, Mul, Sub, SubAssign};

/// A temporary wrapper upgrading arrays with matrix operations
///
/// It is said temporary because the output of the operations won't be wrapped.
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub struct Matrix<T, const M: usize, const N: usize>(pub [[T; N]; M]);
impl<T, const M: usize, const N: usize> Matrix<T, M, N> {
    pub fn unwrap(self) -> [[T; N]; M] {
        let Self(matrix) = self;
        matrix
    }
    pub fn transpose(self) -> [[T; M]; N] {
        self.unwrap().matrix_transpose()
    }
    pub fn map<U, F: FnMut(T) -> U>(self, f: F) -> [[U; N]; M] {
        self.unwrap().matrix_map(f)
    }
    pub fn map_index<U, F: FnMut(T, usize, usize) -> U>(self, f: F) -> [[U; N]; M] {
        self.unwrap().matrix_map_index(f)
    }
}
impl<T, const M: usize, const N: usize> Deref for Matrix<T, M, N> {
    type Target = [[T; N]; M];
    fn deref(&self) -> &Self::Target {
        let Matrix(matrix) = self;
        matrix
    }
}
impl<T, const M: usize, const N: usize> DerefMut for Matrix<T, M, N> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        let Matrix(matrix) = self;
        matrix
    }
}
impl<T, const M: usize, const N: usize> From<[[T; N]; M]> for Matrix<T, M, N> {
    fn from(matrix: [[T; N]; M]) -> Self {
        Self(matrix)
    }
}
impl<T, const M: usize, const N: usize> From<Matrix<T, M, N>> for [[T; N]; M] {
    fn from(matrix: Matrix<T, M, N>) -> Self {
        matrix.unwrap()
    }
}

// ADD_ASSIGN //////////////////////////////////////////////////////////////////
impl<T, U, const M: usize, const N: usize> AddAssign<[[U; N]; M]> for Matrix<T, M, N>
where
    T: AddAssign<U>,
{
    fn add_assign(&mut self, rhs: [[U; N]; M]) {
        self.matrix_add_assign(rhs);
    }
}
impl<T, U, const M: usize, const N: usize> AddAssign<Matrix<U, M, N>> for Matrix<T, M, N>
where
    T: AddAssign<U>,
{
    fn add_assign(&mut self, rhs: Matrix<U, M, N>) {
        self.matrix_add_assign(rhs.unwrap());
    }
}
impl<T, U, const M: usize, const N: usize> AddAssign<Matrix<U, M, N>> for [[T; N]; M]
where
    T: AddAssign<U>,
{
    fn add_assign(&mut self, rhs: Matrix<U, M, N>) {
        self.matrix_add_assign(rhs.unwrap());
    }
}

// SUB_ASSIGN //////////////////////////////////////////////////////////////////
impl<T, U, const M: usize, const N: usize> SubAssign<[[U; N]; M]> for Matrix<T, M, N>
where
    T: SubAssign<U>,
{
    fn sub_assign(&mut self, rhs: [[U; N]; M]) {
        self.matrix_sub_assign(rhs);
    }
}
impl<T, U, const M: usize, const N: usize> SubAssign<Matrix<U, M, N>> for Matrix<T, M, N>
where
    T: SubAssign<U>,
{
    fn sub_assign(&mut self, rhs: Matrix<U, M, N>) {
        self.matrix_sub_assign(rhs.unwrap());
    }
}
impl<T, U, const M: usize, const N: usize> SubAssign<Matrix<U, M, N>> for [[T; N]; M]
where
    T: SubAssign<U>,
{
    fn sub_assign(&mut self, rhs: Matrix<U, M, N>) {
        self.matrix_sub_assign(rhs.unwrap());
    }
}

// ADD /////////////////////////////////////////////////////////////////////////
impl<T, U, const M: usize, const N: usize> Add<[[U; N]; M]> for Matrix<T, M, N>
where
    T: Add<U>,
{
    type Output = [[<T as Add<U>>::Output; N]; M];
    fn add(self, rhs: [[U; N]; M]) -> Self::Output {
        self.unwrap().matrix_add(rhs)
    }
}
impl<T, U, const M: usize, const N: usize> Add<Matrix<U, M, N>> for Matrix<T, M, N>
where
    T: Add<U>,
{
    type Output = [[<T as Add<U>>::Output; N]; M];
    fn add(self, rhs: Matrix<U, M, N>) -> Self::Output {
        self.unwrap().matrix_add(rhs.unwrap())
    }
}
impl<T, U, const M: usize, const N: usize> Add<Matrix<U, M, N>> for [[T; N]; M]
where
    T: Add<U>,
{
    type Output = [[<T as Add<U>>::Output; N]; M];
    fn add(self, rhs: Matrix<U, M, N>) -> Self::Output {
        self.matrix_add(rhs.unwrap())
    }
}

// SUB /////////////////////////////////////////////////////////////////////////
impl<T, U, const M: usize, const N: usize> Sub<[[U; N]; M]> for Matrix<T, M, N>
where
    T: Sub<U>,
{
    type Output = [[<T as Sub<U>>::Output; N]; M];
    fn sub(self, rhs: [[U; N]; M]) -> Self::Output {
        self.unwrap().matrix_sub(rhs)
    }
}
impl<T, U, const M: usize, const N: usize> Sub<Matrix<U, M, N>> for Matrix<T, M, N>
where
    T: Sub<U>,
{
    type Output = [[<T as Sub<U>>::Output; N]; M];
    fn sub(self, rhs: Matrix<U, M, N>) -> Self::Output {
        self.unwrap().matrix_sub(rhs.unwrap())
    }
}
impl<T, U, const M: usize, const N: usize> Sub<Matrix<U, M, N>> for [[T; N]; M]
where
    T: Sub<U>,
{
    type Output = [[<T as Sub<U>>::Output; N]; M];
    fn sub(self, rhs: Matrix<U, M, N>) -> Self::Output {
        self.matrix_sub(rhs.unwrap())
    }
}

// MUL /////////////////////////////////////////////////////////////////////////
impl<T, U, const M: usize, const N: usize, const O: usize> Mul<[[U; O]; N]> for Matrix<T, M, N>
where
    T: Mul<U>,
    T: Clone,
    U: Clone,
    <T as Mul<U>>::Output: Sum,
{
    type Output = [[<T as Mul<U>>::Output; O]; M];
    fn mul(self, rhs: [[U; O]; N]) -> Self::Output {
        self.unwrap().matrix_mul(rhs)
    }
}
impl<T, U, const M: usize, const N: usize, const O: usize> Mul<Matrix<U, N, O>> for Matrix<T, M, N>
where
    T: Mul<U>,
    T: Clone,
    U: Clone,
    <T as Mul<U>>::Output: Sum,
{
    type Output = [[<T as Mul<U>>::Output; O]; M];
    fn mul(self, rhs: Matrix<U, N, O>) -> Self::Output {
        self.unwrap().matrix_mul(rhs.unwrap())
    }
}
impl<T, U, const M: usize, const N: usize, const O: usize> Mul<Matrix<U, N, O>> for [[T; N]; M]
where
    T: Mul<U>,
    T: Clone,
    U: Clone,
    <T as Mul<U>>::Output: Sum,
{
    type Output = [[<T as Mul<U>>::Output; O]; M];
    fn mul(self, rhs: Matrix<U, N, O>) -> Self::Output {
        self.matrix_mul(rhs.unwrap())
    }
}

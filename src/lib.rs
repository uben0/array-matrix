//! Array extension to support matrices operations
//!
//! ```
//! use array_matrix::MatrixExt;
//!
//! let m1 = [
//!     [1, 2, 3],
//!     [4, 5, 6],
//! ];
//! let m2 = [
//!     [1, 2],
//!     [3, 4],
//!     [5, 6],
//! ];
//! let m3 = [
//!     [22, 28],
//!     [49, 64],
//! ];
//!
//! assert_eq!(m1.matrix() * m2, m3);
//! ```

use std::iter::{Product, Sum};
use std::ops::{Add, AddAssign, Mul, Sub, SubAssign};

mod matrix;
pub use matrix::Matrix;
mod matrices;
pub use matrices::Matrices;

pub fn identity<T: Copy, const X: usize>() -> [[T; X]; X]
where
    T: Sum + Product,
{
    let one = std::iter::empty::<T>().product();
    let zero = std::iter::empty::<T>().sum();
    [[(); X]; X].matrix_map_index(|_, m, n| if m == n { one } else { zero })
}

/// Adds a method that temporarely wraps arrays
pub trait MatrixExt<T, const M: usize, const N: usize> {
    fn matrix(self) -> Matrix<T, M, N>;
}

/// Adds a method that permanently wraps arrays
pub trait MatricesExt<T, const M: usize, const N: usize> {
    fn matrices(self) -> Matrices<T, M, N>;
}

/// Extends the array type with matrix operations
pub trait MatrixTrait<T, const M: usize, const N: usize> {
    fn matrix_map<U, F: FnMut(T) -> U>(self, f: F) -> [[U; N]; M];
    fn matrix_map_index<U, F: FnMut(T, usize, usize) -> U>(self, f: F) -> [[U; N]; M];
    fn matrix_transpose(self) -> [[T; M]; N];
    fn matrix_add<U>(self, rhs: impl Into<[[U; N]; M]>) -> [[<T as Add<U>>::Output; N]; M]
    where
        T: Add<U>;
    fn matrix_sub<U>(self, rhs: impl Into<[[U; N]; M]>) -> [[<T as Sub<U>>::Output; N]; M]
    where
        T: Sub<U>;
    fn matrix_add_assign<U>(&mut self, rhs: impl Into<[[U; N]; M]>)
    where
        T: AddAssign<U>;
    fn matrix_sub_assign<U>(&mut self, rhs: impl Into<[[U; N]; M]>)
    where
        T: SubAssign<U>;
    fn matrix_mul<U, const O: usize>(self, rhs: impl Into<[[U; O]; N]>) -> [[<T as Mul<U>>::Output; O]; M]
    where
        T: Mul<U>,
        <T as Mul<U>>::Output: Sum,
        T: Clone,
        U: Clone;
}

impl<T, const M: usize, const N: usize> MatrixExt<T, M, N> for [[T; N]; M] {
    fn matrix(self) -> Matrix<T, M, N> {
        Matrix(self)
    }
}
impl<T, const M: usize, const N: usize> MatricesExt<T, M, N> for [[T; N]; M] {
    fn matrices(self) -> Matrices<T, M, N> {
        Matrices(self)
    }
}

impl<T, const M: usize, const N: usize> MatrixTrait<T, M, N> for [[T; N]; M] {
    fn matrix_map<U, F: FnMut(T) -> U>(self, mut f: F) -> [[U; N]; M] {
        self.map(|line| line.map(|elem| f(elem)))
    }
    fn matrix_map_index<U, F: FnMut(T, usize, usize) -> U>(self, mut f: F) -> [[U; N]; M] {
        let mut m = 0..M;
        self.map(|line| {
            let mut n = 0..N;
            let m = m.next().unwrap();
            line.map(|elem| f(elem, m, n.next().unwrap()))
        })
    }
    fn matrix_transpose(self) -> [[T; M]; N] {
        let mut opt = self.matrix_map(Some);
        [[(); M]; N].matrix_map_index(|_, m, n| opt[n][m].take().unwrap())
    }
    fn matrix_add<U>(self, rhs: impl Into<[[U; N]; M]>) -> [[<T as Add<U>>::Output; N]; M]
    where
        T: Add<U>,
    {
        array_zip(self, rhs.into(), |lhs, rhs| {
            array_zip(lhs, rhs, |lhs, rhs| lhs + rhs)
        })
    }
    fn matrix_sub<U>(self, rhs: impl Into<[[U; N]; M]>) -> [[<T as Sub<U>>::Output; N]; M]
    where
        T: Sub<U>,
    {
        array_zip(self, rhs.into(), |lhs, rhs| {
            array_zip(lhs, rhs, |lhs, rhs| lhs - rhs)
        })
    }
    fn matrix_add_assign<U>(&mut self, rhs: impl Into<[[U; N]; M]>)
    where
        T: AddAssign<U>,
    {
        for (lhs, rhs) in self.iter_mut().zip(std::array::IntoIter::new(rhs.into())) {
            for (lhs, rhs) in lhs.iter_mut().zip(std::array::IntoIter::new(rhs)) {
                *lhs += rhs;
            }
        }
    }
    fn matrix_sub_assign<U>(&mut self, rhs: impl Into<[[U; N]; M]>)
    where
        T: SubAssign<U>,
    {
        for (lhs, rhs) in self.iter_mut().zip(std::array::IntoIter::new(rhs.into())) {
            for (lhs, rhs) in lhs.iter_mut().zip(std::array::IntoIter::new(rhs)) {
                *lhs -= rhs;
            }
        }
    }
    fn matrix_mul<U, const O: usize>(self, rhs: impl Into<[[U; O]; N]>) -> [[<T as Mul<U>>::Output; O]; M]
    where
        T: Mul<U>,
        <T as Mul<U>>::Output: Sum,
        T: Clone,
        U: Clone,
    {
        let rhs = rhs.into();
        [[(); O]; M].matrix_map_index(|_, m, n| {
            (0..N).map(|i| self[m][i].clone() * rhs[i][n].clone()).sum()
        })
    }
}

fn array_zip<T, U, V, F: FnMut(T, U) -> V, const N: usize>(
    lhs: [T; N],
    rhs: [U; N],
    mut f: F,
) -> [V; N] {
    let mut rhs = std::array::IntoIter::new(rhs);
    lhs.map(|e| f(e, rhs.next().unwrap()))
}

#[cfg(test)]
mod tests;

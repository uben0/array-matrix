use super::MatrixExt;

#[test]
fn test_add() {
    let m1 = [[1, 2, 3], [4, 5, 6]];
    let m2 = [[7, 6, 1], [2, 4, 2]];
    let m3 = [[8, 8, 4], [6, 9, 8]];
    assert_eq!(m1.matrix() + m2, m3);
}

#[test]
fn test_map() {
    let m1 = [[1, 2, 3], [4, 5, 6]];
    let m2 = [[2, 4, 6], [8, 10, 12]];
    assert_eq!(m1.matrix().map(|n| n * 2), m2);
}

#[test]
fn test_mul() {
    let m1 = [[1, 2, 3], [4, 5, 6]];
    let m2 = [[9, 1, 9, 2], [-2, -4, -5, 0], [3, 3, 0, 1]];
    let m3 = [[14, 2, -1, 5], [44, 2, 11, 14]];
    assert_eq!(m1.matrix() * m2, m3);
}

#[test]
fn test_transpose() {
    let m1 = [
        [1, 2, 3],
        [4, 5, 6],
    ];
    let m2 = [
        [1, 4],
        [2, 5],
        [3, 6],
    ];
    assert_eq!(m1.matrix().transpose(), m2);
}

use super::{MatricesExt, MatrixTrait};
use std::iter::Sum;
use std::ops::{Add, AddAssign, Deref, DerefMut, Mul, Sub, SubAssign};

/// A permanent wrapper upgrading arrays with matrix operations
///
/// It is said permanent because the output of the operations will be wrapped as well.
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub struct Matrices<T, const M: usize, const N: usize>(pub [[T; N]; M]);
impl<T, const M: usize, const N: usize> Matrices<T, M, N> {
    pub fn unwrap(self) -> [[T; N]; M] {
        let Self(matrix) = self;
        matrix
    }
    pub fn transpose(self) -> Matrices<T, N, M> {
        self.unwrap().matrix_transpose().matrices()
    }
    pub fn map<U, F: FnMut(T) -> U>(self, f: F) -> Matrices<U, M, N> {
        self.unwrap().matrix_map(f).matrices()
    }
    pub fn map_index<U, F: FnMut(T, usize, usize) -> U>(self, f: F) -> Matrices<U, M, N> {
        self.unwrap().matrix_map_index(f).matrices()
    }
}
impl<T, const M: usize, const N: usize> Deref for Matrices<T, M, N> {
    type Target = [[T; N]; M];
    fn deref(&self) -> &Self::Target {
        let Matrices(matrix) = self;
        matrix
    }
}
impl<T, const M: usize, const N: usize> DerefMut for Matrices<T, M, N> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        let Matrices(matrix) = self;
        matrix
    }
}
impl<T, const M: usize, const N: usize> From<[[T; N]; M]> for Matrices<T, M, N> {
    fn from(matrix: [[T; N]; M]) -> Self {
        Self(matrix)
    }
}
impl<T, const M: usize, const N: usize> From<Matrices<T, M, N>> for [[T; N]; M] {
    fn from(matrix: Matrices<T, M, N>) -> Self {
        matrix.unwrap()
    }
}

// ADD_ASSIGN //////////////////////////////////////////////////////////////////
impl<T, U, const M: usize, const N: usize> AddAssign<[[U; N]; M]> for Matrices<T, M, N>
where
    T: AddAssign<U>,
{
    fn add_assign(&mut self, rhs: [[U; N]; M]) {
        self.matrix_add_assign(rhs);
    }
}
impl<T, U, const M: usize, const N: usize> AddAssign<Matrices<U, M, N>> for Matrices<T, M, N>
where
    T: AddAssign<U>,
{
    fn add_assign(&mut self, rhs: Matrices<U, M, N>) {
        self.matrix_add_assign(rhs.unwrap());
    }
}
impl<T, U, const M: usize, const N: usize> AddAssign<Matrices<U, M, N>> for [[T; N]; M]
where
    T: AddAssign<U>,
{
    fn add_assign(&mut self, rhs: Matrices<U, M, N>) {
        self.matrix_add_assign(rhs.unwrap());
    }
}

// SUB_ASSIGN //////////////////////////////////////////////////////////////////
impl<T, U, const M: usize, const N: usize> SubAssign<[[U; N]; M]> for Matrices<T, M, N>
where
    T: SubAssign<U>,
{
    fn sub_assign(&mut self, rhs: [[U; N]; M]) {
        self.matrix_sub_assign(rhs);
    }
}
impl<T, U, const M: usize, const N: usize> SubAssign<Matrices<U, M, N>> for Matrices<T, M, N>
where
    T: SubAssign<U>,
{
    fn sub_assign(&mut self, rhs: Matrices<U, M, N>) {
        self.matrix_sub_assign(rhs.unwrap());
    }
}
impl<T, U, const M: usize, const N: usize> SubAssign<Matrices<U, M, N>> for [[T; N]; M]
where
    T: SubAssign<U>,
{
    fn sub_assign(&mut self, rhs: Matrices<U, M, N>) {
        self.matrix_sub_assign(rhs.unwrap());
    }
}

// ADD /////////////////////////////////////////////////////////////////////////
impl<T, U, const M: usize, const N: usize> Add<[[U; N]; M]> for Matrices<T, M, N>
where
    T: Add<U>,
{
    type Output = Matrices<<T as Add<U>>::Output, M, N>;
    fn add(self, rhs: [[U; N]; M]) -> Self::Output {
        self.unwrap().matrix_add(rhs).matrices()
    }
}
impl<T, U, const M: usize, const N: usize> Add<Matrices<U, M, N>> for Matrices<T, M, N>
where
    T: Add<U>,
{
    type Output = Matrices<<T as Add<U>>::Output, M, N>;
    fn add(self, rhs: Matrices<U, M, N>) -> Self::Output {
        self.unwrap().matrix_add(rhs.unwrap()).matrices()
    }
}
impl<T, U, const M: usize, const N: usize> Add<Matrices<U, M, N>> for [[T; N]; M]
where
    T: Add<U>,
{
    type Output = Matrices<<T as Add<U>>::Output, M, N>;
    fn add(self, rhs: Matrices<U, M, N>) -> Self::Output {
        self.matrix_add(rhs.unwrap()).matrices()
    }
}

// SUB /////////////////////////////////////////////////////////////////////////
impl<T, U, const M: usize, const N: usize> Sub<[[U; N]; M]> for Matrices<T, M, N>
where
    T: Sub<U>,
{
    type Output = Matrices<<T as Sub<U>>::Output, M, N>;
    fn sub(self, rhs: [[U; N]; M]) -> Self::Output {
        self.unwrap().matrix_sub(rhs).matrices()
    }
}
impl<T, U, const M: usize, const N: usize> Sub<Matrices<U, M, N>> for Matrices<T, M, N>
where
    T: Sub<U>,
{
    type Output = Matrices<<T as Sub<U>>::Output, M, N>;
    fn sub(self, rhs: Matrices<U, M, N>) -> Self::Output {
        self.unwrap().matrix_sub(rhs.unwrap()).matrices()
    }
}
impl<T, U, const M: usize, const N: usize> Sub<Matrices<U, M, N>> for [[T; N]; M]
where
    T: Sub<U>,
{
    type Output = Matrices<<T as Sub<U>>::Output, M, N>;
    fn sub(self, rhs: Matrices<U, M, N>) -> Self::Output {
        self.matrix_sub(rhs.unwrap()).matrices()
    }
}

// MUL /////////////////////////////////////////////////////////////////////////
impl<T, U, const M: usize, const N: usize, const O: usize> Mul<[[U; O]; N]> for Matrices<T, M, N>
where
    T: Mul<U>,
    T: Clone,
    U: Clone,
    <T as Mul<U>>::Output: Sum,
{
    type Output = Matrices<<T as Mul<U>>::Output, M, O>;
    fn mul(self, rhs: [[U; O]; N]) -> Self::Output {
        self.unwrap().matrix_mul(rhs).matrices()
    }
}
impl<T, U, const M: usize, const N: usize, const O: usize> Mul<Matrices<U, N, O>>
    for Matrices<T, M, N>
where
    T: Mul<U>,
    T: Clone,
    U: Clone,
    <T as Mul<U>>::Output: Sum,
{
    type Output = Matrices<<T as Mul<U>>::Output, M, O>;
    fn mul(self, rhs: Matrices<U, N, O>) -> Self::Output {
        self.unwrap().matrix_mul(rhs.unwrap()).matrices()
    }
}
impl<T, U, const M: usize, const N: usize, const O: usize> Mul<Matrices<U, N, O>> for [[T; N]; M]
where
    T: Mul<U>,
    T: Clone,
    U: Clone,
    <T as Mul<U>>::Output: Sum,
{
    type Output = Matrices<<T as Mul<U>>::Output, M, O>;
    fn mul(self, rhs: Matrices<U, N, O>) -> Self::Output {
        self.matrix_mul(rhs.unwrap()).matrices()
    }
}
